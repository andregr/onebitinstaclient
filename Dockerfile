FROM node:11.4.0-alpine

ENV INSTALL_PATH /app

RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

RUN npm install -g ionic@3.20.1 cordova

RUN npm install

COPY . .

CMD ["ionic", "serve", "-c"]